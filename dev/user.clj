(ns user
  (:require [portal.api :as portal]
            [arex.system :as system]))

(comment
  (def p (portal/open))

  (add-tap #'portal/submit)

  (require '[clojure.repl.deps :refer [add-lib]])

  (add-lib 'org.clojure/core.match)
  
  (system/start! "config.edn" :dev)
  (system/stop!)
  ,)
