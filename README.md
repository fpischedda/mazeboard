# mazeboard

Clojure implementation of the mazeboard game (previously implemented in common lisp, maybe I'll share that one too some day... :) )

"Mazeboard" is a work in progress board game, with a set of rules which are barely defined:

- The game board is a grid of size NxM (not sure about the ideal dimensions yet)
- Each cell of the grid (or tile if you like) has four walls which may be open or closed
- An open wall give access to the nearby cell/tile
- Each turn a player can either move to a new position or rotate the walls of a tile
- What action is available to the user can be decided by a coin flip (move/rotate), a dice or even a set of cards (I don't know yet)
- The game ends when a user gets to the final position, which may be set at the center of the board, or set randomly around the center (I don't know yet)

## Development

Sources available [here](https://gitlab.com/fpischedda/mazeboard)

## Build

So far the game is running in the browser only, so everyone will use the same
device; network multiplayer will be available soon(ish)(ish).

To run the game and work on it if you like, first setup shadow-cljs on your machine, then run:

    $ npx shadow-cljs watch game

## Tests

Tests are, so far, targeting node because all new logic have been tested in the browser but,
given that no tests is using the DOM or targetting the UI, node is good enough; to work on the game
and have a live update on the test suite, add the test target to the watch argument list:
    $ npx shadow-cljs watch game tests


## Original set of rules

These are the first rules I had in mind, maybe they work maybe not:

- The game is for two to four players.
- Each player must start at one of the four corners of the board.
- The boards is a grid of NxN tiles with N being a odd number equal or greater than 5.
- Each tile represents a room which may have up to four open walls, or walls with an open door.
- Each player can either move through an open wall to one of the four nearby tiles or rotate a tile.
- The first player that reaches the central tile wins the game.

Additional game modes:

* A player flips a coin, head=the player MUST move through an open wall if possible,
  tail=the player MUST rotate a tile.
* A player rolls a dice, 1=rotate clockwise, 6=rotate countercolockwise, 2=move up,
  3=move down, 4=move left, 5=move right.

## License

Copyright © 2015-2024 Francesco Pischedda

Distributed under the AGLP 3 License
# mazeboard-clj
