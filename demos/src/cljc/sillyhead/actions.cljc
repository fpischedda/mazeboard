(ns sillyhead.actions
  "sillyhead.actions namespaces translates actions to events that will then
   update the game state and the client state(s), implementing the sillyhead
   game."
  (:require [clojure.core.match :refer [match]]
            [nano-id.core :refer [nano-id]]))

(defn player-joined-event
  [{players :players} {:keys [name client-id]}]
  [:player-joined {:player {:id (nano-id 4)
                            :client-id client-id
                            :name name}
                   :known-players players}])

(def special-ranks
  #{2 10 11})

(def special-cards
  #{{:rank 3 :suite :hearts}
    {:rank 3 :suite :diams}})

(def see-through-cards
  #{{:rank 3  :suite :hearts}
    {:rank 3  :suite :diams}
    {:rank 11 :suite :hearts}
    {:rank 11 :suite :clubs}
    {:rank 11 :suite :diams}
    {:rank 11 :suite :spades}})

(defn valid-card-fn
  [target-rank]
  (let [cmp-fn (if (= 7 target-rank) <= >=)]
    (fn [{rank :rank :as card}]
      (or (special-ranks rank)
          (special-cards card)
          (cmp-fn rank target-rank)))))

(defn filter-valid-cards [cards target-rank]
  (filter (valid-card-fn target-rank) cards))

(defn first-comparable-discarded-card
  "Return the first comparable card of discard pile; the rule is that
   see through cards like all Js or 'red' threes should be ignored and
   the previous card in the discard pile must be considered.
   If no comparable cards are in the discard pile then return nil."
  [discard-pile]
  (->> discard-pile
       (remove see-through-cards)
       last))

(defn playable-cards
  "Return a list of all playable cards in the provided hand"
  [{:keys [hand table-visible table-hidden]} discard-pile]
  (let [target-rank (if-let [card (first-comparable-discarded-card discard-pile)] 
                   (:rank card)
                   1)]
    (cond
      (seq hand) (filter-valid-cards hand target-rank)
      (seq table-visible) (filter-valid-cards table-visible target-rank)
      :else table-hidden
      )))

(defn round-finished-event
  [{turn :turn}]
  [:round-finished {:current-player (:current-player turn)}])

(defn round-payload
  [current-player round-number hand discard-pile]
  {:current-player current-player
   :round-number   round-number
   :round-direction 1
   :playable-cards (set (playable-cards hand discard-pile))
   })

(defn round-started-event
  [{:keys [turn players-hand discard-pile]}]
  (let [{:keys [current-player round-number]} turn
        hand (nth players-hand current-player)]
    [:round-started {:current-player current-player
                     :round-number   round-number
                     :round-direction 1
                     :playable-cards (set (playable-cards hand discard-pile))
                     }]))

(defn get-next-player
  [current-player direction players]
  (loop [player (mod (+ current-player direction) (count players))]
    (if (get-in players [player :active])
      player
      (recur (mod (+ player direction) (count players))))))

(defn next-round-event
  [{:keys [players turn] :as state}]
  (let [{:keys [current-player round-direction round-number]} turn
        next-player (get-next-player current-player
                                     round-direction
                                     players)]
    (if (= current-player next-player)
      [:game-over {:loser current-player}]
      (round-started-event
       (update state
               :turn
               assoc :current-player next-player :round-number (inc round-number)
               )))))

(defn gen-cards [min-rank max-rank suites generation]
  (flatten 
    (for [suite suites]
      (for [rank (range min-rank (inc max-rank))]
        {:rank rank :suite suite :generation generation}))))

(defn gen-all-cards
  "Return a collection of cards with a rank from min-rank to max-rank
   and for all specified suites, taking into account the number of players,
   generating enough cards, considering that each player needs 9 cards when
   the game starts"
  ([number-of-players] (gen-all-cards number-of-players {}))
  ([number-of-players {:keys [min-rank max-rank suites]
                       :or {min-rank 1
                            max-rank 14
                            suites   [:clubs :spades :hearts :diams]}}]
   (let [generations (+ 1 (int (/ (* 9 number-of-players) 52)))]
     (mapcat (fn [generation] (gen-cards min-rank max-rank suites generation)) (range generations)))
   ))

(def cards-per-player 9)

(defn gen-players-hand
  "Return a vector of the hands for all the players,
   players-cards is a collection of cards to be assigned to
   players, each player must have three hidden cards, three
   visible cards and three cards for the hand"
  [players-cards]
  (reduce
   (fn [acc cards]
     (let [[hidden cards] (split-at 3 cards)
           [visible hand] (split-at 3 cards)]
       (conj acc {:table-hidden (vec hidden)
                  :table-visible (vec visible)
                  :hand (vec hand)})))
   [] (partition cards-per-player players-cards)))

(defn action--start-game->events
  [{players :players}]
  (let [[players-cards cards]
        (->> {:min-rank 2
              :max-rank 14
              :suites [:clubs :spades :hearts :diams]}
             (gen-all-cards (count players))
             shuffle
             (split-at (* cards-per-player (count players))))

        [discard cards] (split-at 1 cards)
        players-hand (gen-players-hand players-cards)]
    [[:game-started {:deck (vec cards)
                     :discard-pile (vec discard)
                     :players-hand players-hand
                     :players (vec players)}]
     [:round-started (round-payload 0 1 (nth players-hand 0) discard)]]))

(defn action--take-discard-pile->events
  [{:keys [discard-pile players-hand] :as state}
   {target :target}]
  (let [players-hand (update-in players-hand [target :hand] concat discard-pile)]
    [[:discard-pile-taken
      {:discard-pile []
       :players-hand players-hand}]
     (round-finished-event state)
     (next-round-event (assoc state :players-hand players-hand :discard-pile []))]))

(defn empty-hand?
  "Return true if the provided hand is empty"
  [{:keys [hand table-visible table-hidden]}]
  (>= 0 (+ (count hand) (count table-visible) (count table-hidden))))

(defn play-card
  "Remove the card from the player hand returning updated all players' hand"
  [players-hand card player]
  (let [rm (fn [cards] (->> cards (remove #(= card %)) vec))
        {:keys [hand table-visible]} (nth players-hand player)]
    (cond
      (seq hand)          (update-in players-hand [player :hand] rm)
      (seq table-visible) (update-in players-hand [player :table-visible] rm)
      :else               (update-in players-hand [player :table-hidden] rm))))

(defn refill-hand-event
  [current-player players-hand deck how-many]
  (let [[refill new-deck] (split-at how-many deck)]
    [:hand-refilled {:target current-player
                     :players-hand (update-in players-hand [current-player :hand] concat refill)
                     :deck (vec new-deck)}]))

(defn action--end-turn->events
  "When finishing the turn, if there are still cards in the deck then
   the players should refill their hand to have at least three cards;
   this is done automatically if the deck is not empty yet, generating
   an event to update the game and possibly UI state.
   At least the finish turn event is emitted."
  [{:keys [players-hand deck turn] :as state}]
  (let [current-player (:current-player turn)
        refill-count
        (- 3 (count (get-in players-hand [current-player :hand])))]
    (cond-> (list (round-finished-event state))
      ;; check if there are cards in the deck and if the hand must be refilled
      (and (> (count deck) 0) (> refill-count 0))
      (conj (refill-hand-event current-player players-hand deck refill-count))
      :finally vec)))

(defn card-played-event
  [{:keys [players-hand discard-pile]} player card]
  [:card-played {:players-hand players-hand
                 :discard-pile discard-pile
                 :card card
                 :player player}])

(defn play-card-wipe-discard->events
  [{:keys [players-hand turn] :as state} card player]
  (let [current-player (:current-player turn)
        events [(card-played-event state card player)]]
    (concat events
            (if (empty-hand? (nth players-hand current-player))
               [[:player-left {:player current-player}]
                (next-round-event state)]
               [(round-started-event state)]))
    ))

(defn play-card-next-player-events
  [{:keys [players-hand turn] :as state} card player]
  (let [end-turn-events (action--end-turn->events state)
        players-hand
        ;; end-turn-events may return a refill event as the second item of the vector
        ;; in that case the players-hand will be the ones taken from that event
        (if-let [hands (get-in end-turn-events [0 1 :players-hands])]
          hands
          players-hand)
        state (assoc state :players-hand players-hand)
        current-player (:current-player turn)
        player-left-fn (fn [events]
                         (if (empty-hand? (nth players-hand current-player))
                           (concat events [[:player-left {:player current-player}]])
                           events))]
    (-> [(card-played-event state card player)]
        (concat end-turn-events)
        player-left-fn
        (concat [(next-round-event state)]))))

(defn action--play-card->events
  "Return events for the card being played:
   - if the played card cannot be removed from the player hand then take the 
     discard pile
   - if the card can be played and the last one makes it the fourth of a 
     kind, then remove the discard pile, the turn goes to the player and can
     play another card
   - if the card can be played and it is the last one then remove the player
     from the game
   - if there is only one player left, then it is the sillyhead!"
  [{:keys [discard-pile players-hand turn] :as state} {:keys [card player]}]
  (let [{:keys [playable-cards]} turn
        new-players-hand (play-card players-hand card player)
        last-card-in-the-pile (first-comparable-discarded-card discard-pile)
        discard-pile (conj discard-pile card)
        card-played-event [:card-played {:players-hand new-players-hand
                                         :discard-pile discard-pile}]
        can-play-another-card? false]
    (cond
      ;; played a card that is not valid (can happen with hidden cards)
      (not (and (playable-cards card) ((valid-card-fn (:rank last-card-in-the-pile)) card)))
      (action--take-discard-pile->events state {:target player})

      ;; rank 10 removes the discard pile from the table
      (= 10 (:rank card))
      (play-card-wipe-discard->events
       (assoc state
              :players-hand new-players-hand
              :discard-pile [])
       card player)

      ;; in case the player can play another card of the same rank
      can-play-another-card? [card-played-event
                              (round-started-event (assoc state
                                                          :players-hand new-players-hand
                                                          :discard-pile discard-pile))]
      :else
      (play-card-next-player-events (assoc state
                                           :players-hand new-players-hand
                                           :discard-pile discard-pile)
                                    card player)
      )
    ))

(defn action->events
  [game action]
  (prn "Received action" action)
  (match action
         [:create-game]            [[:game-created]]
         [:end-turn]               (action--end-turn->events game)
         [:join-game opts]         [(player-joined-event game opts)]
         [:play-card opts]         (action--play-card->events game opts)
         [:start-game]             (action--start-game->events game)
         [:take-discard-pile opts] (action--take-discard-pile->events game opts)))
