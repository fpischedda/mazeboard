(ns mazeboard.game
  (:require #?(:cljs [cljs.core.async :refer [>! <! chan]]
               :clj  [clojure.core.async :refer [>! <! chan]])
            [mazeboard.actions :as actions])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

(defonce state (atom {}))

(defn game-loop
  [actions-chan events-chan]
  (go-loop []
    (let [action (<! actions-chan)
          events (actions/action->events @state action)]
      (swap! state (fn [state] (reduce actions/handle-event state events)))
      (>! events-chan events))
    (recur)))

(defn start-game
  ([]
   (start-game {}))

  ([{:keys [actions-chan events-chan game-id client-id]
     :or
     {actions-chan (chan)
      events-chan (chan)
      game-id "local"
      client-id "local"}}]
   (game-loop actions-chan events-chan)
   (go
     (>! events-chan [[:game-created {:game-id game-id
                                      :client-id client-id}]]))

     [events-chan actions-chan]))
