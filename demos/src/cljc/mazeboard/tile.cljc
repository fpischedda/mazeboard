(ns mazeboard.tile)

(defn make-tile
  "creates a tile represented as a vector index 0 is north, 1 is east etc"
  [north east south west]
  [north east south west])

(defn random-wall
  "randomize a wall; a wall can be :open or :closed"
  []
  (if (> (rand) 0.5) :closed :open))

(defn fix-closed-walls
  "open one tile wall if all are closed"
  [tile]
  (if (some #{:open} tile)
    tile
    (assoc tile (int (* 3.9 (rand))) :open)))

(defn random-tile
  "randomize a tile"
  []
  (fix-closed-walls (make-tile (random-wall) (random-wall) (random-wall) (random-wall))))

(defn rotate-tile-left
  "Rotate a tile counterclockwise"
  [tile]
  (conj (subvec tile 1 4) (first tile)))

(defn rotate-tile-right
  "Rotate a tile clockwise"
  [tile]
  (into (subvec tile 3) (subvec tile 0 3)))

(defn rotate-tile
  "rotate a tile in the specified direction: > 0 = right otherwise left"
  [tile direction]
  (if (= direction :right)
    (rotate-tile-right tile)
    (rotate-tile-left tile)))

(def wall-name {0 :north 1 :east 2 :south 3 :west})

(defn open-walls [walls]
  (->>
   walls
   (map-indexed (fn [idx wall] (when (= :open wall) (wall-name idx))))
   (filter some?)))

(defn wall-at
  "return the tile wall at the specified direction (:north, :east etc)"
  [tile side]
  (nth tile (case side
              :north 0
              :east 1
              :south 2
              :west 3)))

(defn side-open?
  "returns true if the wall at the specified side is open"
  [tile side]
  (= (wall-at tile side) :open))
