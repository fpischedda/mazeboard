(ns mazeboard.board
  (:require [mazeboard.tile :as tile]))

(defn create-board
  "return a new random board with specified dimensions"
  [width height]
  (let [tiles (for [row (range height)
                    col (range width)]
                [{:row row :col col} (tile/random-tile)])]
    {:tiles (into {} tiles)
     :end-position {:row (quot height 2) :col (quot width 2)}
     :height height
     :width width}))

(defn is-inside?
  "returns true if the position is inside the board, false otherwise"
  [board {:keys [row col]}]
  (and
   (>= row 0) (< row (:height board))
   (>= col 0) (< col (:width board))))
