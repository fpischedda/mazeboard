(ns mazeboard.actions
  "mazeboard.actions namespaces translates actions to events that will then update
   the game state and the client state(s)"
  (:require [clojure.core.match :refer [match]]
            [nano-id.core :refer [nano-id]]
            [mazeboard.board :as board]
            [mazeboard.tile :as tile]))

(defn player-joined-event
  [{:keys [players]} {:keys [name client-id]}]
  [:player-joined {:player {:id (nano-id 4)
                            :client-id client-id
                            :name name}
                   :known-players players}])

(defn valid-move-actions
  [target board position]
  (->>
   (get-in board [:tiles position])
   tile/open-walls
   (mapv (fn [direction] [:move-player {:direction direction :target target}]))))

(defn rotate-tiles-actions
  [position]
  [[:rotate-tile {:direction :left :position position}]
   [:rotate-tile {:direction :right :position position}]])

(defn update-position
  [position direction]
  (case direction
    :north (update position :row dec)
    :east (update position :col inc)
    :south (update position :row inc)
    :west (update position :col dec)))

(defn player-moved-event
  [{:keys [players]}
   {:keys [direction target]}]
  (let [old-pos (get-in players [target :position])
        new-pos (update-position old-pos direction)]
    [:player-moved {:target target
                    :old-position old-pos
                    :new-position new-pos}]))

(defn tile-rotated-event
  [game {:keys [position direction]}]
  (let [tile (get-in game [:board :tiles position])]
    [:tile-rotated {:position position
                    :direction direction
                    :old-tile tile
                    :new-tile (tile/rotate-tile tile direction)}]))

(defn round-finished-event
  [{:keys [turn]}]
  [:round-finished {:current-player (:current-player turn)}])

(defn round-started-event
  [{:keys [turn board players]}]
  (let [{:keys [current-player round-number]} turn
        next-player (mod (inc current-player) (count players))
        position (-> players (nth next-player) :position)]
    [:round-started {:current-player next-player
                     :round-number (inc round-number)
                     :actions (into
                               (valid-move-actions next-player board position)
                               (rotate-tiles-actions position))
                     }]))

(defn player-in-end-position
  [game [_ {:keys [new-position target]}]]
  (when (= (get-in game [:board :end-position]) new-position)
    target))

(defn game-over-event
  [winner]
  [:game-over {:winner winner}])

(def starting-positions [{:row 0 :col 0}
                         {:row 0 :col 1}
                         {:row 1 :col 1}
                         {:row 1 :col 0}])

(defn players-starting-positions
  "Return a vector of tuple composed by the player id and its starting
   position"
  [players board-width board-height]
  (->> players
      (map-indexed (fn [idx _player]
                     (let [{:keys [row col]} (nth starting-positions idx)]
                       [idx {:row (* (dec board-height) row)
                             :col (* (dec board-width) col)}])))
      vec))

(defn action--start-game->events
  [{:keys [players]} {:keys [board-width board-height]
                      :or {board-width 7 board-height 7}}]
  (let [board (board/create-board board-width board-height)
        target 0
        positions (players-starting-positions players board-width board-height)
        position (get-in positions [0 1])]
    [[:game-started {:board board
                     :player-positions positions}]
     [:round-started {:current-player target
                      :round-number 1
                      :actions (concat
                                (valid-move-actions target board position)
                                (rotate-tiles-actions position))
                      }]]))

(defn action--move-player->events
  [game opts]
  (let [moved-event (player-moved-event game opts)]
    
    [(round-finished-event game)
     moved-event
     (if-let [player (player-in-end-position game moved-event)]
       (game-over-event player)
       (round-started-event game))]))

(defn action->events
  [game action]
  (prn "Received action" action)
  (match action
         [:create-game opts] [[:game-created opts]]
         [:join-game opts]   [(player-joined-event game opts)]
         [:move-player opts] (action--move-player->events game opts)
         [:rotate-tile opts] [(round-finished-event game)
                              (tile-rotated-event game opts)
                              (round-started-event game)]
         [:start-game opts] (action--start-game->events game opts)))

(defn set-player-positions
  [game positions]
  (update game :players
          (fn [players]
            (reduce (fn [acc [idx position]]
                      (assoc-in acc [idx :position] position))
                    (vec players) positions))))

(defn handle-event--game-started
  [game {:keys [board player-positions]}]
  (-> game
      (assoc :board board)
      (set-player-positions player-positions)))

(defn handle-event--player-joined
  [game {player :player}]
  (update game :players conj player))

(defn handle-event--player-moved
  [game {:keys [target new-position]}]
  (assoc-in game [:players target :position] new-position))

(defn handle-event--round-finished
  [game]
  (assoc-in game [:turn :actions] []))

(defn handle-event--round-started
  [game turn]
  (assoc game :turn turn))

(defn handle-event--tile-rotated
  [game {:keys [position new-tile]}]
  (assoc-in game [:board :tiles position] new-tile))

(defn handle-event
  [game event]
  (prn "Received event " event)
  (match event
         [:game-created opts]    (assoc game
                                        :players []
                                        :game-id (:game-id opts "local")
                                        :client-id (:client-id opts "local"))
         [:game-over opts]       (assoc game :winner {:winner opts})
         [:game-started opts]    (handle-event--game-started game opts)
         [:player-joined opts]   (handle-event--player-joined game opts)
         [:player-moved opts]    (handle-event--player-moved game opts)
         [:round-finished _opts] (handle-event--round-finished game)
         [:round-started opts]   (handle-event--round-started game opts)
         [:tile-rotated opts]    (handle-event--tile-rotated game opts)))
