(ns mazeboard.test-data)

(def test-walls
  [[[:closed :open :open :closed] [:closed :open :open :closed] [:closed :closed :open :closed] [:open :closed :open :closed] [:open :closed :open :closed]]
   [[:open :open :open :closed] [:closed :open :open :closed] [:closed :closed :closed :open] [:open :closed :open :closed] [:open :closed :open :closed]]
   [[:open :open :open :closed] [:closed :closed :open :closed] [:open :closed :closed :closed] [:open :closed :closed :closed] [:open :closed :closed :closed]]
   [[:open :open :open :closed] [:closed :closed :open :closed] [:open :closed :closed :closed] [:closed :closed :open :closed] [:open :closed :closed :closed]]
   [[:open :open :open :closed] [:closed :closed :open :closed] [:open :closed :closed :closed] [:open :open :closed :closed] [:open :open :closed :closed]]])

(defn players-at-position
  "returns all players at the specified coordinates"
  [players position]
  (for [[idx player] (map-indexed (fn [idx p] [idx p]) players)
        :when (= (:position player) position)]
    idx))

(defn create-board
  "return a test board given a 2D map of tiles"
  [{:keys [walls players current-player end-position actions]}]
  (let [height (count walls)
        width (count (nth walls 0))
        current-player-pos (get-in players [current-player :position])
        tiles
        (for [row (range height)
              col (range width)]
          (let [pos {:row row :col col}
                tile {:actions (if (= pos current-player-pos) actions [])
                      :walls (-> walls (nth row) (nth col))
                      :position pos
                      :end-position? (= pos end-position)
                      :players (set (players-at-position players pos))}]
            [pos tile]))]
    {:tiles (into {} tiles)
     :end-position end-position
     :height height
     :width width}))

(defn create-game
  []
  (let [players [{:name "one" :position {:row 0 :col 0} :class "player-1"}
                 {:name "two" :position {:row 0 :col 1} :class "player-2"}]
        current-player 0
        end-position {:row 1 :col 1}
        actions [{:on-click [[:move-player {:direction :east :target 0}]]
                  :action-class "action-move-east"}
                 {:on-click [[:move-player {:direction :south :target 0}]]
                  :action-class "action-move-south"}]]
    {:players players
     :board (create-board {:walls test-walls
                           :players players
                           :current-player current-player
                           :end-position end-position
                           :actions actions})
     :turn {:current-player current-player
            :round-number 1}}))
