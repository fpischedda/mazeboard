(ns mazeboard.ui.core
  (:require [arex-client.core :refer [join-game-flow start-game-flow]]
            [arex-client.utils :refer [get-game-id-from-url]]
            [dumdom.core :as d]
            [mazeboard.game :as game-logic]
            [mazeboard.ui.components.app :as app]
            [mazeboard.ui.event-handler :as events]))

(defonce ui-state (atom {:game {:client-id "local"
                                :players {}
                                :state :game-state/initial
                                :turn {:round-number 0
                                       :current-player 0}
                                :board {:width 0
                                        :height 0
                                        :end-position {:row 0 :col 0}
                                        :tiles {}}}}))

(defn render [state]
  (d/render (app/App state)
            (js/document.getElementById "app")))

(defn events-handler
  [events]
  (swap! ui-state update :game events/handle-events events))

(defn init
  "Initialize a new game, setting up communication channels.
   Games can be run locally in the same browser or networked,
   dispatching command and events via a transport protocol (for now limited to HTTP)"
  []
  (add-watch ui-state :app (fn [_ _ _ state] (render state)))

  (if-let [game-id (get-game-id-from-url)]
    ;; if a game id is found try connecting to it
    (join-game-flow game-id events/handle-event)

    ;; otherwise let the user choose to start a local or remote game
    (do
      (start-game-flow game-logic/start-game events-handler)
      (swap! ui-state assoc :state :game-state/select-game-mode))))
