(ns mazeboard.ui.components.game
  (:require
   [dumdom.core :refer [defcomponent] :as d]))

(defcomponent Tile [{:keys [actions position end-position? players]}
                    all-players
                    {:keys [is-client-turn?]}]
  (cond-> [:div.tile {:class "tile-background"
                      :key position}

           ;; Render available actions, if the client is not handling
           ;; the current player then disable the buttons
           (for [{:keys [on-click action-class]} actions]
             [:div.action (if is-client-turn?
                            {:class [action-class "action-active"]
                             :on-click on-click}
                            {:class action-class})])

           (for [player players]
             (let [{:keys [class name]} (get all-players player)]
               [:div.player {:class class} name]))]

    end-position?
    (conj [:div.end-position])))

(defcomponent Row [row width tiles players turn]
  [:div.board-row {:key row}
   (for [col (range width)]
     (Tile (get tiles {:row row :col col}) players turn))])

(defcomponent Board [{:keys [tiles width height]} players turn]
  [:div.board-container
   [:div.board {:key (:round-number turn)}
    (for [row (range height)]
      (Row row width tiles players turn))]])

(defcomponent CurrentTurn
  [{:keys [current-player round-number]} players]
  (when-let [player-name (get-in players [current-player :name])]
    [:div
     [:p (str "Round number " round-number)]
     [:div.current-player [:b player-name] " is your turn!"]]))

(defcomponent JoinGame
  [players client-id game-id]
  [:div.players-joining
   [:div.players-joined (str "Players (" (count players) ")")
    (for [{:keys [id class name]} players] 
      [:div.player {:class class
                    :key id}
       name])
    [:div
     (when (> (count players) 1)
       [:button {:on-click [[:start-game {:board-width 7 :board-height 7}]]} "Start!"])]]
   [:div {:class ["center" "join-game"]}
    [:div
     [:div.center (str "Game id " game-id)]
     [:hr]
     [:div.avatar "Your Name"]
     [:div.name
      [:input {:type :text :id :player-name :placeholder "Your name"}]
      [:button {:on-click
                (fn [e]
                  (let [name (-> "player-name" js/document.getElementById (. -value))]
                    (d/dispatch-event-data e [[:join-game {:name name :client-id client-id}]])))}
       "Join"]]]]
   ])

(defcomponent StartGame
  []
  [:div.game-start
   [:div.game-start-local
    [:button {:on-click [:start-game :game-type/local]} "Start local game"]]
   [:div.game-start-remote
    [:button {:on-click [:start-game :game-type/remote]} "Start remote game"]]
   [:div.game-join-remote
    [:input {:type :text :id :game-id :placeholder "Insert game id"}]
    [:button {:on-click
              (fn [e]
                (let [game-id (-> "game-id"
                                  js/document.getElementById
                                  (. -value))]
                  (d/dispatch-event-data e [:join-game game-id])))}
     "Join remote game"]]])

(defcomponent Game
  [{:keys [board players turn state winner client-id game-id]}]
  (case state
    :game-state/initial (StartGame)
    :game-state/created (JoinGame (vals players) client-id game-id)
    :game-state/started
    [:div.game
     (CurrentTurn turn players)
     (Board board players turn)]
    :game-state/finished
    [:div.winner
     [:p (str "The winner is " (get-in players [winner :name]) "!")]]))
