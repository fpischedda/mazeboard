(ns mazeboard.ui.components.app
  (:require
   [dumdom.core :refer [defcomponent]]
   [mazeboard.ui.components.game :as game]))

(defcomponent App [{game :game}]
  [:div
   [:p.header "Mazeboard! (alpha)"]
   (if game
     (game/Game game)
     [:p "No game yet!"])])
