(ns mazeboard.ui.event-handler
  (:require [clojure.core.match :refer [match]]))

(defn game-tiles->ui-tiles
  [tiles end-position]
  (reduce-kv
   (fn [new-tiles pos walls]
     (assoc new-tiles pos {:actions []
                           :walls walls
                           :position pos
                           :end-position? (= pos end-position)
                           :players #{}}))
   {} tiles))

(defn set-player-positions
  [game positions]
  (reduce (fn [acc [player-id position]]
            (-> acc
                (update-in [:board :tiles position :players] conj player-id)
                (assoc-in [:players player-id :position] position)))
          game positions))

(defn event--game-started
  [game {:keys [player-positions board]}]
  (-> game
      (assoc :board board :state :game-state/started)
      (update-in [:board :tiles] game-tiles->ui-tiles (:end-position board))
      (set-player-positions player-positions)))

(defn know-players->map
  [known-players]
  (->> known-players
       (map-indexed (fn [idx p] [idx p]))
       (reduce (fn [acc [idx player]]
                 (assoc acc
                        idx
                        (assoc player :class (str "player-" (inc idx)))))

               {})))

(defn event--player-joined
  [game {:keys [player known-players]}]
  (assoc game :players (know-players->map (conj known-players player))))

(defn event--player-moved
  [game {:keys [target old-position new-position]}]
  (-> game
      (update-in [:board :tiles old-position :players] disj target)
      (update-in [:board :tiles new-position :players] conj target)
      (assoc-in [:players target :position] new-position)
      ))

(defn game-action->ui-action
  [[action opts]]
  (case action
      :move-player {:on-click [[action opts]]
                    :action-class (str "action-move-" (name (:direction opts)))}
      :rotate-tile {:on-click [[action opts]]
                    :action-class (str "action-rotate-" (name (:direction opts)))}))

(defn game-actions->ui-actions
  [game {:keys [actions current-player]}]
  (let [player-pos (get-in game [:players current-player :position])
        ui-actions (mapv game-action->ui-action actions)]
    (assoc-in game [:board :tiles player-pos :actions] ui-actions)))

(defn event--round-finished
  [game {:keys [current-player]}]
  (let [position (get-in game [:players current-player :position])]
    (assoc-in game [:board :tiles position :actions] [])))

(defn setup-turn
  [{:keys [players client-id] :as game} {:keys [current-player] :as turn}]
  (let [is-client-turn? (= client-id
                           (get-in players [current-player :client-id]))
        turn (-> turn
                 (dissoc :actions)
                 (assoc :is-client-turn? is-client-turn?))]
    (assoc game :turn turn)))

(defn event--round-started
  [game turn]
  (-> game
      (setup-turn turn)
      (game-actions->ui-actions turn)))

(defn event--tile-rotated
  [game {:keys [position new-tile]}]
  (assoc-in game [:board :tiles position :walls] new-tile))

(defn handle-event
  [game event]
  (prn "Handling event" event)
  (match event
         [:game-created opts]
         (assoc game :state :game-state/created
                :game-id (:game-id opts "local")
                :client-id (:client-id opts "local"))
         [:game-over opts] (assoc game
                                  :winner (:winner opts)
                                  :state :game-state/finished)
         [:game-started opts] (event--game-started game opts)
         [:player-joined opts] (event--player-joined game opts)
         [:player-moved opts] (event--player-moved game opts)
         [:round-finished opts] (event--round-finished game opts)
         [:round-started opts] (event--round-started game opts)
         [:tile-rotated opts] (event--tile-rotated game opts)))

(defn handle-events
  [game events]
  (reduce handle-event game events))
