(ns sillyhead.core
  (:require [dumdom.core :as d]
            [arex-client.core :refer [join-game-flow start-game-flow]]
            [arex-client.utils :refer [get-game-id-from-url]]
            [sillyhead.ui.components :as c]
            [sillyhead.ui.event-handler :as events]
            [sillyhead.game :as game]))

(defonce ui-state
  (atom
   {:client-id "local"
    :game-id "local"
    :players []
    :state :game-state/initial}))

(defn render [state]
  (prn "Rendering" state)
  (d/render (c/App state)
            (js/document.getElementById "app")))

(defn init []
  (add-watch ui-state :app (fn [_ _ _ state] (render state)))

  (if-let [game-id (get-game-id-from-url)]
    ;; if a game id is found try connecting to it
    (join-game-flow game-id events/handle-event)

    ;; otherwise let the user choose to start a local or remote game
    (do
      (start-game-flow game/start-game
                       (partial swap! ui-state events/handle-events))
      (swap! ui-state assoc :state :game-state/select-game-mode)))
  )
