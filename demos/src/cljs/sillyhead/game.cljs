(ns sillyhead.game
  (:require [cljs.core.async :refer [>! <! chan]]
            [clojure.core.match :refer [match]]
            [sillyhead.actions :as actions])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

(defonce state (atom {}))

(defn handle-event--game-started
  [game {:keys [deck discard-pile players players-hand]}]
  (assoc game
         :deck deck
         :discard-pile discard-pile
         :players (mapv #(assoc % :active true) players)
         :players-hand players-hand
         :turn {:round-number 0
                :round-direction 1
                :current-player 0
                :playable-cards #{}}))

(defn handle-event--player-left
  [state {player :player}]
  (assoc-in state [:players player :active] false))

(defn handle-event--player-joined
  [game {player :player}]
  (update game :players conj player))

(defn handle-event--round-started
  [game turn]
  (assoc game :turn turn))

(defn handle-event--discard-pile-taken
  [state {:keys [discard-pile players-hand]}]
  (-> state
      (assoc :discard-pile discard-pile
             :players-hand players-hand)))

(defn handle-event--card-played
  [state {:keys [players-hand discard-pile]}]
  (-> state
      (assoc :discard-pile discard-pile
             :players-hand players-hand)))

(defn handle-event--hand-refilled
  [state {:keys [players-hand deck]}]
  (-> state
      (assoc :deck deck
             :players-hand players-hand)))

(defn handle-event
  [game event]
  (prn "[Game state] Received event " event)
  (match event
   [:card-played opts] (handle-event--card-played game opts)
   [:discard-pile-taken opts] (handle-event--discard-pile-taken game opts)
   [:game-created]        (assoc game :players [])
   [:game-over opts]      (assoc game :loser {:loser opts})
   [:game-started opts]   (handle-event--game-started game opts)
   [:hand-refilled opts]  (handle-event--hand-refilled game opts)
   [:player-left opts]    (handle-event--player-left game opts)
   [:player-joined opts]  (handle-event--player-joined game opts)
   [:round-finished _]    game
   [:round-started opts]  (handle-event--round-started game opts)))

(defn game-loop
  [actions-chan events-chan]
  (go-loop []
    (let [action (<! actions-chan)
          events (actions/action->events @state action)]
      (swap! state (fn [state] (reduce handle-event state events)))
      (>! events-chan events))
    (recur)))

(defn start-game
  ([]
   (start-game {}))

  ([{:keys [actions-chan events-chan game-id client-id]
     :or
     {actions-chan (chan)
      events-chan (chan)
      game-id "local"
      client-id "local"}}]
   (game-loop actions-chan events-chan)
   (go (>! events-chan [[:game-created {:game-id game-id
                                        :client-id client-id}]]))
   [events-chan actions-chan]))
