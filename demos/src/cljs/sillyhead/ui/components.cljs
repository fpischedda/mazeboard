(ns sillyhead.ui.components
  (:require [dumdom.core :refer [defcomponent] :as d]))

(def special-rank
  {11 "j"
   12 "q"
   13 "k"
   14 "a"})

(defn get-rank-class
  [rank]
  (str "rank-" (get special-rank rank rank)))

(def special-rank-display
  {11 "J"
   12 "Q"
   13 "K"
   14 "A"})

(defn get-rank-display
  [rank]
  (get special-rank-display rank rank))

(def suites {:hearts "♥"
             :diams  "♦"
             :spades "♠"
             :clubs  "♣"})

(def transparent-cards
  #{{:rank 3  :suite :hearts}
    {:rank 3  :suite :diams}
    {:rank 11 :suite :hearts}
    {:rank 11 :suite :clubs}
    {:rank 11 :suite :diams}
    {:rank 11 :suite :spades}})

(defcomponent Card
  ([card]
   (Card card nil))
  ([{:keys [suite rank]} action]
   [(if action :a :div)
    {:key (str suite "-" rank "-" (some? action))
     :class ["card"
             (get-rank-class rank)
             (name suite)]
     :on-click [action]}
    [:span.rank (get-rank-display rank)]
    [:span.suit (suites suite)]]))

(defcomponent TransparentCard
  ([card]
   (TransparentCard card nil))
  ([{:keys [suite rank]} action]
   [(if action :a :div)
    {:style {:margin-bottom "32px" :opacity "0.5"}
     :class ["card"
             (get-rank-class rank)
             (name suite)]
     :on-click [action]}
    [:span.rank (get-rank-display rank)]
    [:span.suit (suites suite)]]))

(defcomponent CardBack
  ([]
   (CardBack nil))
  ([action]
   [(if action :a :div)
    (cond-> {:class ["card" "back"]}
      (some? action) (assoc :on-click [action]))
    "*"])
  )

(defcomponent Hand
  [{:keys [hand table show-hand current-player name player-idx is-out?]}
   playable-cards]
  [:div.player-hand
   (cond->
       [:span (if current-player {:style {:background-color "yellow"}} {})
        name]
       is-out?
       [:span {:style {:background-color "green"}} "Is SAFE!"])
   
   [:div
    [:div.playingCards {:key (str name "-" current-player)
                        :style {:marging-left "2em"}} 
     [:ul.hand
      (for [card hand]
        [:li (if show-hand
               (Card card (when (playable-cards card)
                            [:play-card {:card card :player player-idx}]))
               (CardBack))])]
     
     [:table
      [:tbody
       [:tr
        (for [{:keys [hidden visible]} table]
          [:td {:style {:width "6em"}}
           [:div.playingCards
            (cond-> [:ul.hand]
              (some? hidden)
              (conj [:li (CardBack (when (playable-cards hidden)
                                     [:play-card {:card hidden
                                                  :player player-idx}]))])
              (some? visible)
              (conj [:li (Card visible (when (playable-cards visible)
                                         [:play-card {:card visible
                                                      :player player-idx}]))]))]])
        ]]]]
    ]])

(defcomponent PlayersHand
  [players-hand playable-cards]
  [:div.players-hand
   (for [hand players-hand]
     (Hand hand playable-cards))])

(defcomponent Deck
  [{:keys [deck discard-pile]} current-player is-current-player]
  (let [action
        (when is-current-player
          [:take-discard-pile {:target current-player}])]
    [:div.playingCards
     [:table
      [:tbody
       [:tr
        [:td {:style {:width "8em"}}
         [:ul.deck
          (for [_ (range (count deck))]
            [:li (CardBack)])]]
        [:td {:style {:width "6em"}}
         [:ul.deck
          (for [card discard-pile]
            [:li
             (if (transparent-cards (select-keys card [:rank :suite]))
               (TransparentCard card action)
               (Card card action))])]]]]]]))

(defcomponent WaitingGameStarted
  [players game-id]
  [:div.waiting-game-started
   [:p (str "Game ID " game-id)]
   [:div (str "Players (" (count players) ")")
    (for [{:keys [id name]} players] 
      [:div {:key id} name])
    [:div [:p "Waiting for other players"]]
    [:div
     (if (> (count players) 1)
       [:button {:on-click [[:start-game]]} "Start!"]
       [:p "At least two players are required to start the game"])]]
   ])

(defcomponent JoinRemoteGame
  [client-id game-id]
  [:div {:class ["center" "join-game"]}
    [:div
     [:div.center (str "Game id " game-id)]
     [:hr]
     [:div.avatar "Your Name"]
     [:div.name
      [:input {:type :text :id :player-name :placeholder "Your name"}]
      [:button {:on-click
                (fn [e]
                  (let [name (-> "player-name" js/document.getElementById (. -value))]
                    (d/dispatch-event-data e [[:join-game {:name name :client-id client-id}]])))}
       "Join"]]]])

(defcomponent JoinLocalGame
  [players client-id]
  [:div.players-joining
   [:div.players-joined (str "Players (" (count players) ")")
    (for [{:keys [id class name]} players] 
      [:div.player {:class class
                    :key id}
       name])
    [:div
     (when (> (count players) 1)
       [:button {:on-click [[:start-game]]} "Start!"])]]
   [:div {:class ["center" "join-game"]}
    [:div {:style {:padding "8em"}}
     [:div.avatar "Your Name"]
     [:div.name
      [:input {:type :text :id :player-name :placeholder "Your name"}]
      [:button {:on-click
                (fn [e]
                  (let [name (-> "player-name" js/document.getElementById (. -value))]
                    (d/dispatch-event-data e [[:join-game {:name name :client-id client-id}]])))}
       "Join"]]]]
   ])

(defcomponent Board
  [{:keys [table players-hand]}
   playable-cards
   current-player
   is-current-player]
  [:div {:style {:background-color "darkgreen"}}
   (Deck table current-player is-current-player)
   (PlayersHand players-hand playable-cards)])

(defcomponent SelectGameMode
  []
  [:div
   [:div
    [:p "Play Sillyhead"]
    [:div.playingCards
     [:ul.hand
      [:li (Card {:rank 11 :suite :clubs})]
      [:li (Card {:rank 12 :suite :diams})]
      [:li (Card {:rank 13 :suite :spades})]
      [:li (Card {:rank 14 :suite :hearts})]]]]
   [:div.select-game-mode
    [:div.game-start-local
     [:button {:on-click [:start-game :game-type/local]} "Start local game"]]
    [:div.game-start-remote
     [:button {:on-click [:start-game :game-type/remote]} "Start remote game"]]
    [:div.game-join-remote
     [:input {:type :text :id :game-id :placeholder "Insert game id"}]
     [:button {:on-click
               (fn [e]
                 (let [game-id (-> "game-id"
                                   js/document.getElementById
                                   (. -value))]
                   (d/dispatch-event-data e [:join-game game-id])))}
      "Join remote game"]]]])

(defcomponent App
  [{:keys [state board current-player is-current-player players loser client-id game-id playable-cards]}]
  (case state
    :game-state/select-game-mode (SelectGameMode)
    :game-state/created (if (= game-id "local")
                          (JoinLocalGame players client-id)
                          (JoinRemoteGame client-id game-id))
    :game-state/waiting-game-started (WaitingGameStarted players game-id)
    :game-state/started (Board board playable-cards current-player is-current-player)
    :game-state/finished
    [:div.winner
     [:p (str "Contratulations " (get-in players [loser :name]) "!")]
     [:p "You are the silly head until next time!"]]))
