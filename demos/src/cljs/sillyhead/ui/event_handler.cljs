(ns sillyhead.ui.event-handler
  (:require [clojure.core.match :refer [match]]))

(defn top-up-to
  [coll up-to value]
  (let [how-many (- up-to (count coll))]
    (if (> how-many 0)
      (reduce (fn [acc v] (conj acc v)) coll (take how-many (repeat value)))
      coll)))

(defn event-hand->ui-hand
  [{:keys [hand table-hidden table-visible]}
   player
   client-id]
  {:show-hand (= client-id (:client-id player))
   :name (:name player)
   :player-idx (:player-idx player)
   :current-player false
   :hand hand
   :table
   (let [table-hidden (top-up-to table-hidden 3 nil)
         table-visible (top-up-to table-visible 3 nil)]
     (mapv (fn [hidden visible]
             {:hidden hidden
              :visible visible})
           table-hidden table-visible))})

(defn event->board
  [{:keys [deck discard-pile players-hand]} players client-id]
  {:table {:deck deck :discard-pile discard-pile}
   :players-hand
   (mapv (fn [hand player]
           (event-hand->ui-hand hand player client-id))
         players-hand players)})

(defn handle-event--game-started
  [state {players :players :as opts}]
  (let [players
        (->> players
             (map-indexed (fn [idx p] (assoc p :player-idx idx)))
             vec)]
    (assoc state
           :state :game-state/started
           :players players
           :board (event->board opts players (:client-id state)))))

(defn handle-event--player-left
  [state {player :player}]
  (assoc-in state [:players player :is-out?] true))

(defn handle-event--player-joined
  [{client-id :client-id :as state} {:keys [player known-players]}]
  (let [state (assoc state :players (conj (or known-players []) player))]
    (if (or
         (= client-id "local")
         (not= client-id (:client-id player)))
      state
      (assoc state :state :game-state/waiting-game-started))))

(defn handle-event--round-finished
  [state {current-player :current-player}]
  (-> state
   (assoc :playable-cards #{}
          :current-player current-player
          :is-current-player false)
   (assoc-in [:board :players-hand current-player :current-player] false)))

(defn handle-event--round-started
  [{:keys [client-id players] :as state}
   {:keys [current-player round-number playable-cards]}]
  (-> state
   (assoc :round-number round-number
          :playable-cards playable-cards
          :current-player current-player
          :is-current-player (= client-id
                                (get-in players [current-player :client-id])))
   (assoc-in [:board :players-hand current-player :current-player] true)))

(defn handle-event--card-played
  [{:keys [client-id players] :as state} {:keys [discard-pile players-hand]}]
  (-> state
      (assoc-in [:board :table :discard-pile] discard-pile)
      (assoc-in [:board :players-hand]
                (mapv (fn [hand player]
                        (event-hand->ui-hand hand player client-id))
                      players-hand players))))

(defn handle-event--discard-pile-taken
  [{:keys [client-id players] :as state} {:keys [discard-pile players-hand]}]
  (-> state
      (assoc-in [:board :table :discard-pile] discard-pile)
      (assoc-in [:board :players-hand]
                (mapv (fn [hand player]
                        (event-hand->ui-hand hand player client-id))
                      players-hand players))))

(defn handle-event--hand-refilled
  [{:keys [client-id players] :as state} {:keys [deck players-hand]}]
  (-> state
      (assoc-in [:board :table :deck] deck)
      (assoc-in [:board :players-hand]
                (mapv (fn [hand player]
                        (event-hand->ui-hand hand player client-id))
                      players-hand players))))

(defn handle-event
  [state event]
  (prn "Event" event)
  (prn "State" state)
  (match event
   [:card-played opts]        (handle-event--card-played state opts)
   [:discard-pile-taken opts] (handle-event--discard-pile-taken state opts)
   [:game-created opts]       (assoc state
                                     :state :game-state/created
                                     :game-id (:game-id opts "local")
                                     :client-id (:client-id opts "local"))
   [:game-over opts]          (assoc state :state :game-state/finish :loser (:loser opts))
   [:game-started opts]       (handle-event--game-started state opts)
   [:hand-refilled opts]      (handle-event--hand-refilled state opts)
   [:player-left opts]        (handle-event--player-left state opts)
   [:player-joined opts]      (handle-event--player-joined state opts)
   [:round-finished opts]     (handle-event--round-finished state opts)
   [:round-started opts]      (handle-event--round-started state opts)))

(defn handle-events
  [state events]
  (reduce handle-event state events))
