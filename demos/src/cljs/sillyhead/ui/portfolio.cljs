(ns sillyhead.ui.portfolio
  (:require [portfolio.ui :as ui]
            [sillyhead.ui.scenes]))

::sillyhead.ui.scenes

(defn init
  []
  (ui/start! {:config {:css-paths ["/css/cards.css" "/css/style.css"]}}))
