(ns sillyhead.ui.scenes
  (:require [portfolio.dumdom :refer [defscene]]
            [sillyhead.ui.components :as c]))

(defscene deck
  (c/Deck {:deck    [{:rank 6  :suite :spades}
                     {:rank 11 :suite :hearts }
                     {:rank 3  :suite :diams}
                     {:rank 4  :suite :clubs}]
           :discard [{:rank 8  :suite :clubs}]}))

(defscene hand-current-player
  (c/Hand {:name "Player 1"
           :show-hand true
           :current-player true
           :hand [{:rank 7  :suite :hearts}
                  {:rank 8  :suite :clubs}
                  {:rank 12 :suite :spades}]
           :table [{:hidden  {:rank 3  :suite :clubs}
                    :visible {:rank 13 :suite :diams}}
                   {:hidden  {:rank 8  :suite :hearts}
                    :visible {:rank 2  :suite :spades}}
                   {:hidden  {:rank 10 :suite :clubs}
                    :visible {:rank 9  :suite :clubs}}]}
          #{{:rank 7 :suite :hearts}}))

(defscene board
  (c/Board {:table {:deck [{:rank 6  :suite :spades}
                           {:rank 11 :suite :hearts }
                           {:rank 3  :suite :diams}
                           {:rank 4  :suite :clubs}]
                    :discard [{:rank 8  :suite :clubs}]}
            :players-hand [{:show-hand true
                            :current-player true
                            :name "Player one"
                            :hand [{:rank 7  :suite :hearts}
                                   {:rank 8  :suite :diams}
                                   {:rank 12 :suite :spades}]
                            :table [{:hidden  {:rank 3  :suite :clubs}
                                     :visible {:rank 13 :suite :diams}}
                                    {:hidden  {:rank 8  :suite :hearts}
                                     :visible {:rank 2  :suite :spades}}
                                    {:hidden  {:rank 10 :suite :clubs}
                                     :visible {:rank 9  :suite :clubs}}]}
                           {:show-hand false
                            :current-player false
                            :name "Player two"
                            :hand [{:rank 5  :suite :spades}
                                   {:rank 14 :suite :clubs}
                                   {:rank 10 :suite :hearts}]
                            :table [{:hidden  {:rank 4  :suite :diams}
                                     :visible {:rank 6  :suite :diams}}
                                    {:hidden  {:rank 8  :suite :spades}
                                     :visible {:rank 3  :suite :hearts}}
                                    {:hidden  {:rank 11 :suite :clubs}
                                     :visible {:rank 9  :suite :hearts}}]}]}
           #{{:rank 7 :suite :hearts}}))

(defscene app--startup-selection
  (c/App {:state :game-state/select-game-mode}))

(defscene app--join-game
  (c/App {:state :game-state/created
          :players {0 {:name "One"}}
          :client-id "local"
          :game-id "local"}))
