(ns mazeboard.actions-test
  (:require
   #?(:cljs [cljs.test :refer [deftest is testing]]
      :clj  [clojure.test :refer [deftest is testing]])

   [mazeboard.actions :as sut]))

(def base-game {:players [{:id "client-1" :name "one" :position {:row 0 :col 0}}
                          {:id "client-2" :name "two" :position {:row 0 :col 1}}]
                :turn {:current-player 0
                       :round-number 1
                       :actions [[:move-player {:direction :east}]]}})

(def base-game-with-board
  (assoc base-game :board
         {:width 2
          :height 2
          :tiles {{:row 0 :col 0} [:closed :open :closed :closed]
                  {:row 0 :col 1} [:closed :closed :open :closed]}}))

(deftest action->events
  (testing "Action :join-game should generate :player-joined event"
    (let [[[event {:keys [player known-players]}]]
          (sut/action->events {:players []}
                              [:join-game {:name "player"
                                           :client-id "client-1"}])]
      (is (and
           (= :player-joined event)
           (= [] known-players)
           (= "client-1" (:client-id player))
           (= "player" (:name player))
           (some? (:id player))))))

  (testing
      "Action :start-game should generate :game-started, :round-started event"
    (let [[[game-started-event {:keys [board player-positions]}]
           [round-started-event {:keys [current-player round-number actions]}]]
          (sut/action->events base-game [:start-game {:board-width 2
                                                      :board-height 3}])]
      (is (and
           (= round-started-event :round-started)
           (= current-player 0)
           (= round-number 1)
           (> (count actions) 0)
           
           (= game-started-event :game-started)
           (= player-positions [[0 {:row 0 :col 0}]
                                [1 {:row 0 :col 1}]])
           (= 2 (:width board))
           (= 3 (:height board))))))

  (testing
      "Action :move-player should generate :round-finished, :player-moved and
     :round-started events:
     - :round-finished should cointain which player finished the round
     - :player-moved should contain which player moved, the old and new position
     - :round-started should contain the active player, round number and the available actions"
      (is (= [[:round-finished {:current-player 0}]
              [:player-moved {:target 0
                              :old-position {:row 0 :col 0}
                              :new-position {:row 0 :col 1}}]
              [:round-started {:current-player 1
                               :round-number 2
                               :actions [[:move-player {:direction :south :target 1}]
                                         [:rotate-tile {:direction :left :position {:row 0 :col 1}}]
                                         [:rotate-tile {:direction :right :position {:row 0 :col 1}}]]}]]

             (sut/action->events
              base-game-with-board
              [:move-player {:target 0 :direction :east}]))))

  (testing
      "Action :move-player should generate a :game-over event when the player moves to the
    final position"
      (is (= [[:round-finished {:current-player 0}]
              [:player-moved {:target 0
                              :old-position {:row 0 :col 0}
                              :new-position {:row 0 :col 1}}]
              [:game-over {:winner 0}]]

             (sut/action->events
              (assoc-in base-game-with-board [:board :end-position] {:row 0 :col 1})
              [:move-player {:target 0 :direction :east}]))))
  
  (testing
      "Action :rotate-tile should generate :round-finisehd, :tile-rotated and
     :round-started events:
     - :round-finished should contain which player finished the round
     - :tile-rotated event should contain the coordinates of the tile, the old and new tile walls
     - :round-started should contain the active player, round number and the available actions"
      (is (= [[:round-finished {:current-player 0}]
              [:tile-rotated {:position {:row 0 :col 0}
                              :direction :right
                              :old-tile [:closed :open :closed :closed]
                              :new-tile [:closed :closed :open :closed]}]
              [:round-started {:current-player 1
                               :round-number 2
                               :actions [[:move-player {:direction :south :target 1}]
                                         [:rotate-tile {:direction :left :position {:row 0 :col 1}}]
                                         [:rotate-tile {:direction :right :position {:row 0 :col 1}}]]}]]

             (sut/action->events
              base-game-with-board
              [:rotate-tile {:position {:row 0 :col 0}
                             :direction :right}]))))

  (testing
      "Action :rotate-tile should generate :round-finisehd, :tile-rotated and
     :round-started events, same the the previous one but rotating left"
      (is (= [[:round-finished {:current-player 0}]
              [:tile-rotated {:position {:row 0 :col 0}
                              :direction :left
                              :old-tile [:closed :open :closed :closed]
                              :new-tile [:open :closed :closed :closed]}]
              [:round-started {:current-player 1
                               :round-number 2
                               :actions [[:move-player {:direction :south :target 1}]
                                         [:rotate-tile {:direction :left :position {:row 0 :col 1}}]
                                         [:rotate-tile {:direction :right :position {:row 0 :col 1}}]]}]]

             (sut/action->events
              base-game-with-board
              [:rotate-tile {:position {:row 0 :col 0}
                             :direction :left}])))))
