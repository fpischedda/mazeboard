(ns mazeboard.ui.event-handler-test
  (:require
   [cljs.test :refer [deftest is testing]]
   [mazeboard.board :as board]
   [mazeboard.ui.event-handler :as sut]))

(def test-game {:turn {:round-number 1
                       :current-player 1}
                :client-id "one"
                :players {0 {:name "one" :client-id "one" :position {:row 0 :col 0}}
                          1 {:name "two" :client-id "two" :position {:row 1 :col 0}}}
                :board {:tiles {{:row 0 :col 0} {:actions []
                                                 :players #{0}
                                                 :walls [:open :open :open :open]}
                                {:row 1 :col 0} {:actions []
                                                 :players #{1}
                                                 :walls [:open :closed :open :open]}}}})

(deftest event--game-created
  (is (= {:state :game-state/created :game-id "local" :client-id "local"}
         (sut/handle-event {} [:game-created {}]))))

(deftest event--game-started
  (let [game
        (sut/handle-event {} [:game-started
                              {:board (board/create-board 2 2)
                               :player-positions [["one" {:row 0 :col 0}]
                                                  ["two" {:row 0 :col 1}]]}])]
    (is (and
         (= (:state game) :game-state/started)
         (= (-> game :board :width) 2)
         (= (-> game :board :height) 2)
         (= (get-in game [:board :tiles {:row 0 :col 0} :players]) #{"one"})
         (= (get-in game [:board :tiles {:row 0 :col 1} :players]) #{"two"})
         (= (get-in game [:players "one" :position]) {:row 0 :col 0})
         (= (get-in game [:players "two" :position]) {:row 0 :col 1})))
    ))

(deftest event--game-over
  (is (= {:state :game-state/finished
          :winner 1}
         (sut/handle-event {} [:game-over {:winner 1}]))))

(deftest event--player-joined
  (let [new-game
        (assoc test-game :players {0 {:id 0
                                      :client-id "client-3"
                                      :name "three"
                                      :class "player-1"}})]
    (is (= new-game
           (sut/handle-event
            test-game
            [:player-joined {:player {:id 0
                                      :client-id "client-3"
                                      :name "three"}
                             :known-players []}])))))

(deftest event--player-moved
  (let [new-game (-> test-game
                     (assoc-in [:players 1 :position] {:row 1 :col 1})
                     (update-in [:board :tiles {:row 1 :col 0} :players] disj 1)
                     (update-in [:board :tiles {:row 1 :col 1} :players] conj 1))]
    (is (= new-game
           (sut/handle-event test-game
                             [:player-moved {:target 1
                                             :old-position {:row 1 :col 0}
                                             :new-position {:row 1 :col 1}}])))))

(deftest event--round-started
  (testing "Round started, is client turn"
    (let [new-game (-> test-game
                       (assoc :turn {:round-number 2
                                     :current-player 0
                                     :is-client-turn? true})
                       (assoc-in [:board :tiles {:row 0 :col 0} :actions]
                                 [{:on-click [[:move-player {:direction :east :target 0}]]
                                   :action-class "action-move-east"}
                                  {:on-click [[:rotate-tile {:direction :right}]]
                                   :action-class "action-rotate-right"}]))]
      (is (= new-game
             (sut/handle-event test-game
                               [:round-started
                                {:round-number 2
                                 :current-player 0
                                 :actions [[:move-player {:direction :east :target 0}]
                                           [:rotate-tile {:direction :right}]]}])))))

  (testing "Round started, is NOT client turn"
    (let [new-game (-> test-game
                       (assoc :turn {:round-number 2
                                     :current-player 1
                                     :is-client-turn? false})
                       (assoc-in [:board :tiles {:row 1 :col 0} :actions]
                                 [{:on-click [[:move-player {:direction :east :target 1}]]
                                   :action-class "action-move-east"}
                                  {:on-click [[:rotate-tile {:direction :right}]]
                                   :action-class "action-rotate-right"}]))]
      (is (= new-game
             (sut/handle-event test-game
                               [:round-started
                                {:round-number 2
                                 :current-player 1
                                 :actions [[:move-player {:direction :east :target 1}]
                                           [:rotate-tile {:direction :right}]]}]))))))
