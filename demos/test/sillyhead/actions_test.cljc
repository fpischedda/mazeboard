(ns sillyhead.actions-test
  (:require [clojure.test :refer [deftest testing is]]
            [sillyhead.actions :as sut]))

(deftest gen-all-cards
  (testing "All cards are generated correctly"
    (is (= [{:rank 2 :suite :hearts :generation 0}
            {:rank 3 :suite :hearts :generation 0}
            {:rank 2 :suite :spades :generation 0}
            {:rank 3 :suite :spades :generation 0}]
           (vec (sut/gen-all-cards 1 {:min-rank 2
                                      :max-rank 3
                                      :suites [:hearts :spades]}))))))

(deftest gen-players-hand
  (testing "Cards are distributed evenly to all players"
    (let [cards (sut/gen-all-cards 2 {:min-rank 2
                                      :max-rank 10
                                      :suites [:hearts :spades]})]
      (is (= [{:table-hidden [{:rank 2 :suite :hearts :generation 0}
                              {:rank 3 :suite :hearts :generation 0}
                              {:rank 4 :suite :hearts :generation 0}]
               :table-visible [{:rank 5 :suite :hearts :generation 0}
                               {:rank 6 :suite :hearts :generation 0}
                               {:rank 7 :suite :hearts :generation 0}]
               :hand [{:rank 8 :suite :hearts :generation 0}
                      {:rank 9 :suite :hearts :generation 0}
                      {:rank 10 :suite :hearts :generation 0}]}
              {:table-hidden [{:rank 2 :suite :spades :generation 0}
                              {:rank 3 :suite :spades :generation 0}
                              {:rank 4 :suite :spades :generation 0}]
               :table-visible [{:rank 5 :suite :spades :generation 0}
                               {:rank 6 :suite :spades :generation 0}
                               {:rank 7 :suite :spades :generation 0}]
               :hand [{:rank 8  :suite :spades :generation 0}
                      {:rank 9  :suite :spades :generation 0}
                      {:rank 10 :suite :spades :generation 0}]}]
             (sut/gen-players-hand cards))))))

(deftest action--start-game
  (testing "Should create :game-started and :round-started events"
    (is (= [:game-started :round-started]
           (mapv first
                 (sut/action--start-game->events {:players [{:id 1} {:id 2}]}))))))

(deftest round-finished-event
  (testing "Should create :round-finished event with the current player index"
    (is (= [:round-finished {:current-player 1}]
           (sut/round-finished-event {:turn {:current-player 1}})))))

(defn player-hand
  [{:keys [hand table-visible table-hidden]
    :or {hand [{:rank 6 :suite :hearts}]
         table-visible [{:rank 6 :suite :spades}]
         table-hidden [{:rank 6 :suite :clubs}]}}]
  {:hand hand
   :table-visible table-visible
   :table-hidden table-hidden})

(deftest generate-playable-cards
  (testing "No card can be played"
    (is (= '()
           (sut/playable-cards (player-hand {}) [{:rank 8 :suite :diams}]))))

  (testing "Can play card in hand"
    (is (= '({:rank 6, :suite :hearts})
           (sut/playable-cards (player-hand {}) [{:rank 5 :suite :diams}]))))

  (testing "Can't play card in table visible"
    (is (= '()
           (sut/playable-cards (player-hand {:hand []}) [{:rank 8 :suite :diams}]))))

  (testing "Can play card in table visible"
    (is (= '({:rank 6, :suite :spades})
           (sut/playable-cards (player-hand {:hand []}) [{:rank 5 :suite :diams}]))))

  (testing "Hidden cards can be played regardless of what we have on the board"
    (is (= '({:rank 6, :suite :clubs})
           (sut/playable-cards (player-hand {:hand [] :table-visible []})
                               [{:rank 10 :suite :diams}]))))

  (testing "Can play card, testing rank 7"
    (is (= '({:rank 6, :suite :hearts})
           (sut/playable-cards (player-hand {})
                               [{:rank 7 :suite :diams}]))))

  (testing "Can't play card, testing rank 7"
    (is (= '()
           (sut/playable-cards (player-hand {:hand [{:rank 8 :suite :hearts}]})
                               [{:rank 7 :suite :diams}]))))
  )

(deftest round-payload
  (testing "Generating round payload for :round-started-event"
    (is (= {:current-player 1
            :round-number 2
            :round-direction 1
            :playable-cards #{{:rank 6, :suite :hearts}}}
           (sut/round-payload 1 2 (player-hand {}) [{:rank 5 :suite :diams}])))))

(deftest next-round-event
  (testing "Should generate :round-started event"
    (let [players-hand [(player-hand {}) (player-hand {:hand []})]]
      (is (= [:round-started {:current-player 1
                              :round-number 2
                              :round-direction 1
                              :playable-cards #{{:rank 6, :suite :spades}}}]
             (sut/next-round-event {:turn {:current-player 0
                                           :round-number 1
                                           :round-direction 1}
                                    :players [{:name "one" :active true}
                                              {:name "two" :active true}]
                                    :players-hand players-hand
                                    :deck []
                                    :discard-pile [{:rank 5 :suite :diams}]}
                                   ))))))

(deftest play-card
  (testing "Playable card is removed from player's hand"
    (is (= [(player-hand {:hand []})]
           (sut/play-card [(player-hand {})] {:rank 6 :suite :hearts} 0))))

  (testing "Playable card is removed from player's visible table"
    (is (= [(player-hand {:hand [] :table-visible []})]
           (sut/play-card [(player-hand {:hand []})]
                          {:rank 6 :suite :spades} 0))))

  (testing "Playable card is removed from player's hidden table"
    (is (= [(player-hand {:hand [] :table-visible [] :table-hidden []})]
           (sut/play-card [(player-hand {:hand [] :table-visible []})]
                          {:rank 6 :suite :clubs} 0)))))

(deftest refill-hand-event
  (testing "Cards are added the the players hand and removed from the deck"
    (is (= [:hand-refilled
            {:target 0
             :players-hand [(player-hand {:hand [{:rank 6 :suite :hearts}
                                                 {:rank 2 :suite :clubs}
                                                 {:rank 3 :suite :diams}]})]
             :deck [{:rank 4 :suite :spades}]}]
           (sut/refill-hand-event 0
                                  [(player-hand {})]
                                  [{:rank 2 :suite :clubs}
                                   {:rank 3 :suite :diams}
                                   {:rank 4 :suite :spades}]
                                  2)))))

(deftest first-comparable-discarded-card
  (testing "Return nil on empty pile"
    (is (nil? (sut/first-comparable-discarded-card []))))

  (testing "Return the last card in the pile"
    (is (= {:rank 5 :suite :diams}
           (sut/first-comparable-discarded-card [{:rank 3 :suite :spades}
                                                 {:rank 3 :suite :hearts}
                                                 {:rank 5 :suite :diams}]))))

  (testing "Return a card removing transparent ones"
    (is (= {:rank 3 :suite :spades}
           (sut/first-comparable-discarded-card [{:rank  5 :suite :diams}
                                                 {:rank  3 :suite :spades}
                                                 {:rank  3 :suite :hearts}
                                                 {:rank 11 :suite :clubs}])))))

(deftest empty-hand?
  (testing "Hand should be empty"
    (is (sut/empty-hand? (player-hand {:hand [] :table-visible [] :table-hidden []}))))
  
  (testing "Hand should not be empty"
    (is (not (sut/empty-hand? (player-hand {:hand []}))))))

(deftest get-next-player
  (testing "Should return the next player considering :active flag"
    (is (= 1 (sut/get-next-player 2 1 [{:active false} {:active true} {:active true}])))))
