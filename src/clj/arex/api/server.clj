(ns arex.api.server
  (:require [manifold.bus :as b]
            [manifold.stream :as s]
            [muuntaja.core :as m]
            [nano-id.core :refer [nano-id]]
            [reitit.coercion.spec :as rcs]
            [reitit.ring :as ring]
            [reitit.ring.coercion :as rrc]
            [reitit.ring.middleware.exception :as exception]
            [reitit.ring.middleware.muuntaja :as muuntaja]
            [reitit.ring.middleware.parameters :as parameters]
            [reitit.swagger :as swagger]
            [reitit.swagger-ui :as swagger-ui]
            [reitit.dev.pretty :as pretty]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [com.brunobonacci.mulog :as u]
            [aleph.http :as http]))

(defonce SSE-headers {:content-type "text/event-stream"
                      :cache-control "no-cache"
                      "X-Accel-Buffering" "no"
                      "Access-Control-Allow-Origin" "*"})

(defonce event-bus (b/event-bus))

(def running-games_ (atom {}))

(def gen-client-id (partial nano-id 8))
(def gen-game-id (partial nano-id 10))
(def gen-game-secret (partial nano-id 12))

(defn create-game
  []
  (let [master-id (gen-client-id)
        game-id (gen-game-id)
        game {:id game-id
              :actions-subscription-secret (gen-game-secret)
              :actions-stream nil
              :master-client-id master-id
              :events-topic (str "events-" game-id)
              :clients #{master-id}}]
    (swap! running-games_ assoc (:id game) game)
    game))

(def base-url "/api/game")

(def game-not-found-response {:status :404})

(defn start-network-game
  [_request]
  (let [{game-id :id
         master-id :master-client-id
         secret :actions-subscription-secret}
        (create-game)]
    (u/log :start-network-game :game-id game-id :client-id master-id :secret secret)
    {:status 201
     :body {:client-id                master-id
            :game-id                  game-id
            :join-url                 (format "%s/%s/join" base-url game-id)
            :events-url               (format "%s/%s/events" base-url game-id)
            :events-broadcast-url     (format "%s/%s/events/%s" base-url game-id secret)
            :actions-subscription-url (format "%s/%s/actions/%s" base-url game-id secret)
            :actions-url              (format "%s/%s/actions" base-url game-id)}}))

(defn format-sse-event
  [payload]
  (format "data: %s\n\n" (prn-str payload)))

(defn forward-action
  [request]
  (let [game-id (get-in request [:path-params :id])
        payload (:body-params request "no event")]

    (if-let [stream (get-in @running-games_ [game-id :actions-stream])]
      (do
        (u/log :forward-action :game-id game-id :payload payload)
        (s/put! stream (format-sse-event payload))
        {:status 201})
      game-not-found-response)))

(defn subscribe-to-actions
  [request]
  (let [{game-id :id sent-secret :secret} (:path-params request)
        secret (get-in @running-games_ [game-id :actions-subscription-secret])
        actions-stream (s/stream)]
    (cond
      (empty? secret)
      game-not-found-response

      (not= secret sent-secret)
      (do
        (u/log :subscribe-to-actions :game-id game-id :success false :error :secret-mismatch)
        {:status 403
         :body {:error "You are not hosting the game and cannot subsribe to actions"}})

      :else
      (do
        (u/log :subscribe-to-actions :game-id game-id :success true)
        (swap! running-games_ assoc-in [game-id :actions-stream] actions-stream)
        {:status 200
         :headers SSE-headers
         :body actions-stream}))))

(defn subscribe-to-events
  [request]
  (let [game-id (-> request :path-params :id)]
    (u/log :subscribe-to-events :game-id game-id)
    (if-let [events-topic (get-in @running-games_ [game-id :events-topic])]
      (do
        (u/log :subscribe-to-events :game-id game-id :game-exists true :headers SSE-headers :events-topic events-topic)
        {:status 200
         :headers SSE-headers
         :body (b/subscribe event-bus events-topic)})
      (do
        (u/log :subscribe-to-events :game-id game-id :game-exists false)
        game-not-found-response))))

(defn broadcast-event-to-subscribers
  [request]
  (let [{game-id :id sent-secret :secret} (:path-params request)
        {secret :actions-subscription-secret topic :events-topic}
        (@running-games_ game-id)
        event (:body-params request)]
    (u/log :broadcast-event :game-id game-id :event event :topic topic :secret secret)
    (cond
      (empty? secret)
      game-not-found-response

      (not= secret sent-secret)
      (do
        (u/log :broadcast-event :game-id game-id :event event :success false :error :secret-mismatch)
        {:status 403
         :body {:error "You are not hosting the game and cannot broadcast events"}})

      :else
      (do
        (u/log :broadcast-event :game-id game-id :event event :topic topic :success true)
        (b/publish! event-bus topic (format-sse-event event))
        {:status 201}))))

(defn join-game
  [request]
  (let [game-id (-> request :path-params :id)
        client-id (gen-client-id)]
    (if (@running-games_ game-id)
      (do
        (u/log :join-game :game-id game-id :game-exists true :client-id client-id)
        (swap! running-games_ update-in [game-id :clients] conj client-id)
        {:status 200
         :body {:client-id   client-id
                :events-url  (format "%s/%s/events" base-url game-id)
                :actions-url (format "%s/%s/actions" base-url game-id)}})
      game-not-found-response)))

(comment

  (join-game {:path-params {:id "rOUOzji72Z"}})
  @running-games_
  ,)

(defn request->endpoint-name
  [request]
  (-> request :reitit.core/match :data (get (:request-method request)) :name))

(defn request->log-context
  [request]
  (-> request
      (select-keys [:request-method :path-params :params :body-params :uri :cookies])
      (assoc :endpoint-name (request->endpoint-name request))))

(defn wrap-log-context
  [handler]
  (fn [request]
    (u/with-context (request->log-context request)
      (handler request))))

(defn gen-app []
  (ring/ring-handler
   (ring/router
    [[""
      ["/swagger.json" {:no-doc true
                        :get (swagger/create-swagger-handler)}]
      ["/api" {:coercion rcs/coercion
               :middleware [swagger/swagger-feature
                            ;; query-params & form-params
                            parameters/parameters-middleware
                            ;; content-negotiation
                            muuntaja/format-negotiate-middleware
                            ;; encoding response body
                            muuntaja/format-response-middleware
                            ;; exception handling
                            exception/exception-middleware
                            ;; decoding request body
                            muuntaja/format-request-middleware
                            ;; coercing response bodys
                            rrc/coerce-response-middleware
                            ;; coercing request parameters
                            rrc/coerce-request-middleware
                            #(wrap-cors %
                                        :access-control-allow-origin [#".*"]
                                        :access-control-allow-credentials "true"
                                        :access-control-allow-methods [:get :put :post :delete])]}

       ["/docs/*" {:no-doc true
                   :get (swagger-ui/create-swagger-ui-handler)}]

       ["/game"
        ["/" {:post {:doc "Setup a game to be played over network"
                     :handler start-network-game}}]
        ["/:id/join" {:doc "Handles clients joining a network game"
                      :get join-game}]
        ["/:id/events"
         {:get {:doc "SSE endpoint to subscribe to, to get game related events"
                :handler subscribe-to-events}}]
        ["/:id/events/:secret"
         {:post {:doc "Broadcast event to all subscribers"
                 :handler broadcast-event-to-subscribers}}]
        ["/:id/actions"
         {:post {:doc "Take the client action and forward it to the server"
                 :handler forward-action}}]
        ["/:id/actions/:secret"
         {:get {:doc "Subscribe to user actions"
                :handler subscribe-to-actions}}]]]]]

    {:exception pretty/exception
     :data {:muuntaja m/instance
            :middleware [wrap-params
                         wrap-keyword-params
                         muuntaja/format-middleware
                         wrap-log-context]}})))

(defn start!
  [config]
  (http/start-server (gen-app)
                     config))

(defn stop!
  [server]
  (.close server))
