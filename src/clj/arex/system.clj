(ns arex.system
  (:require [clojure.java.io :as io]
            [integrant.core :as ig]
            [aero.core :refer [read-config]]
            [com.brunobonacci.mulog :as u]
            [arex.api.server :as server])
  
  (:gen-class))

(def system_ (atom nil))

(defmethod ig/init-key :arex/http [_ config]
  :start (server/start! config))

(defmethod ig/halt-key! :arex/http [_ server]
  (server/stop! server))

(defmethod ig/init-key :arex/logging [_ config]
  :start (u/start-publisher! config))

(defn start!
  [config-filename profile]

  (println (format "Starting AREX service configuration file %s, profile %s" config-filename profile))

  (let [configuration (-> config-filename
                          io/resource
                          (read-config {:profile profile}))]
    (->> configuration
         ig/init
         (reset! system_))
    (println (format "System configuration %s" configuration))))

(defn stop!
  []
  (println "Stopping AREX service")
  (swap! system_ ig/halt!))

(defn -main [& _args]
  (start! "config.edn" :production))

(comment
  (start! "config.edn" :dev)
 
  (stop!)
  ,)
