(ns arex-client.utils
  (:require [clojure.string :refer [includes?]]))

(defn get-origin
  []
  (let [origin (.. js/document -location -origin)]
    (if (includes? origin "localhost")
      "http://localhost:8000"
      "https://getarex.com")))

(defonce base-url (delay (get-origin)))

(defn get-join-url
  [game-id]
  (str @base-url "/api/game/" game-id "/join"))

(defn sub-path->url
  [path]
  (str @base-url path))

(defn url->game-id
  "Return the game id extracting it from the provided URL, if no match is found return nil"
  [url]
  (when-let [match (re-find #"game/(\w+)/?" url)]
    (nth match 1)))

(defn get-game-id-from-url
  "Return the game id extracting it from the browser URL, if no match is found return nil"
  []
  (url->game-id (.. js/window -location -href)))
