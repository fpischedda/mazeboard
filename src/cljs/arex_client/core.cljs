(ns arex-client.core
  (:require [cljs.core.async :refer [>! <! put! chan]]
            [clojure.core.match :refer [match]]
            [clojure.edn :as edn]
            [arex-client.utils :refer [get-join-url sub-path->url]]
            [dumdom.core :as d]
            [kitchen-async.promise :as p]
            [lambdaisland.fetch :as fetch])
  
  (:require-macros [cljs.core.async.macros :refer [go-loop]]))


(defn game-loop
  "Update the game state according to the events received from the
   events channel"
  [events-chan event-hander-fn]
  (go-loop []
    (let [events (<! events-chan)]
      (event-hander-fn events))
    (recur)))

(defn forward-user-actions-to-channel
  "Set dumdom event handler to forward actions to the provided
   actions channel"
  [actions-chan]
  (js/console.log "Setting up dumdom event handler to forward actions")
  (d/set-event-handler!
     (fn [_e actions]
       (doseq [action actions]
         (put! actions-chan action)))))

(defn send-action
  [actions-url action]
  (fetch/post actions-url {:body action
                           :accept "application/transit+json"}))

(defn setup-actions-channel
  "Return a new output channel to which send user actions that
   will be forwarded to the actions endpoint"
  [client-id actions-url]
  (js/console.log "actions setup" client-id actions-url)
  (let [c (chan)]
    (go-loop []
      (let [action (<! c)]
        (js/console.log "Sending action" action "to" actions-url)
        (send-action actions-url action))
      (recur))
    c))

(defn connect-to-sse-events
  "Connect an input channel to SSE events from the provided url,
   returning the EventSource instance"
  [url channel]
  (let [source (js/EventSource. url)]
    (set! (. source -onload)
          (fn [_]
            (js/console.log "SSE connected" url)))
    (set! (. source -onmessage)
          (fn [e]
            (let [data (. e -data)]
              (js/console.log "SSE message" e "data" data)
              (put! channel (edn/read-string data)))))
    (set! (. source -onerror)
          (fn [error]
            (js/console.error "SSE error" url error)))
    source))

(defn get-sse-events-channel
  "Returns a new input channel from which to receive SSE events"
  [events-url]
  (js/console.log "events SSE source setup" events-url)
  (let [c (chan)]
    (connect-to-sse-events events-url c)
    c))

(defn join-game-flow
  "The user joined a remote game, this is what is going to happen:
   - setup events broadcast channel
   - connect to events SSE to receive game events
   - setup actions channel to forward user actions"
  [game-id event-handler-fn]
  (p/let [res (fetch/get (get-join-url game-id))
          {:keys [client-id events-url actions-url]} (js->clj (:body res))]

    (let [events-chan (get-sse-events-channel (sub-path->url events-url))
          actions-chan (setup-actions-channel client-id (sub-path->url actions-url))]
      (js/console.log "Setting game state to created")
      (put! events-chan [[:game-created {:game-id game-id
                                         :client-id client-id}]])
      (forward-user-actions-to-channel actions-chan)
      (game-loop events-chan event-handler-fn)))
  )

(defn start-local-game
  [start-game-fn event-handler-fn]
  (let [[events-chan actions-chan] (start-game-fn)]
    (forward-user-actions-to-channel actions-chan)

    (game-loop events-chan event-handler-fn)))

(defn get-events-broadcast-chan
  "Return a channel to which push game related events.
   An Async loop will consume the channel, broadcasting
   events to all connected clients and updating the game
   state locally by forwarding the events to the provided
   local-events-chan."
  [event-broadcast-url local-events-chan]
  (let [c (chan)]
    (go-loop []
      (let [events (<! c)]
        (fetch/post event-broadcast-url {:body events
                                         :accept "application/transit+json"})
        (>! local-events-chan events))
      (recur))
    c))

(defn start-remote-game
  "The user decided to start a remote game, this is what is going to happen:
   - create a new game room
   - connect to actions SSE to receive user actions
   - connect to events SSE to receive game events
   - setup events broadcast channel
   - setup actions channel to forward user actions"
  [start-game-fn event-handler-fn]
  (p/let [res (fetch/post (sub-path->url "/api/game/"))
          {:keys [game-id client-id events-broadcast-url actions-url actions-subscription-url]}
          (:body res)]

    (let [;; connect to the user actions SSE forwarding them to the returned
          ;; chan, user actions will come from this channel
          actions-listener-chan
          (get-sse-events-channel (sub-path->url actions-subscription-url))

          ;; send user generated action to the message bus' `actions` endpoint
          ;; via the the returned `actions-chan` channel
          ;; this is not strictly needed in this case because "we" are the
          ;; game master process but it helps to have a consistent experience
          actions-chan
          (setup-actions-channel client-id (sub-path->url actions-url))

          ;; create a normal events chan to feed the UI state changes
          events-chan (chan)

          ;; events generated by the game logic must be broadcasted
          ;; sending them to this channel which also forward them to the local
          ;; events chan to feed UI changes
          events-broadcast-chan
          (get-events-broadcast-chan (sub-path->url events-broadcast-url) events-chan)]

      ;; finally game master specific logic
      
      ;; set the dumdom event handler to forward actions to the actions chan
      (forward-user-actions-to-channel actions-chan)

      ;; start a game providing
      ;; the actions and events channels connected to the message bus (SSE)
      (start-game-fn {:actions-chan actions-listener-chan
                      :events-chan  events-broadcast-chan
                      :game-id      game-id
                      :client-id    client-id})

      (game-loop events-chan event-handler-fn))))

(defn start-game-flow
  "Prepare the game state to let the user to chose to start either a local
   or a remote game"
  [start-game-fn event-handler-fn]
  (d/set-event-handler!
   (fn [_e action]
     (match action
            [:start-game :game-type/remote]
            (start-remote-game start-game-fn event-handler-fn)
            [:start-game :game-type/local]
            (start-local-game start-game-fn event-handler-fn)
            [:join-game game-id]
            (join-game-flow game-id event-handler-fn)))))

