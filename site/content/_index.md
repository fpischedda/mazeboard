+++
author = "fp@fpsd.codes"
+++

# Asynchronous Remote Events eXchange.

AREX tries to provide an easy to use and lightweight communication channel for
collaborative/multiplayer applications, based on simple hypermedia and SSE endpoints.

The general abstraction is the `game` room to which clients connect to send and receive data.
AREX does not make any assumption of the format or encoding of the data being exchanged,
this is the resposibility of the client applications.

AREX assumes that there is one master client that handles the shared application state by
receiving commands (or actions) from other clients and broadcasts events to its peers so that
they can update their local state and show changes to the users.

## Why

Some time ago I was working on an experimental multiplayer game, abandoned after I've
reached the point to implement the communication layer in the frontend; later on I have
worked on a simple collaborative web app with which I have learned how to deal with simple
communication flows, mainly broadcasting state changes to the clients.  

From that experience I've learned that the same approach could be extended and generalized,
so that applications with the same requirements could be built on top of a common communication
abstraction. To prove this (to myself, mainly) I've rebooted the boardgame which can now be 
played as intended.

## Demo

First demo application [here](/demo), [sources](https://gitlab.com/fpischedda/mazeboard/demos), `mazeboard` namespace.  
Second demo application [here](/sillyhead), [sources](https://gitlab.com/fpischedda/mazeboard/demos), `sillyhead` namespace.  
Yeah yeah the repo is called mazeboard and not AREX, the reason is that it is the name 
of the boardgame that started everything.

## Common flows

### Start game flow
![start-game-flow](/images/client-server-architecture-start-game.png)

### Join game flow
![jon-game-flow](/images/client-server-architecture-join-game.png)

