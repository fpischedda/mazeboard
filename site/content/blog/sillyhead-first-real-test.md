+++
title = "Sillyhead, testing with real players"
author = "FPSD"
date = 2024-06-14
tags = "arex"
+++

# Testing with real people

A couple of days ago I was attending a meeting that was ending much earlier than
expected (yes, it happens!) so, given that the attendees had already booked their
time, I've asked to try them to stay a bit longer to test the game out together.

## First impressions

The process to start, share and join a game is too long and unnecessarily complicated:

The host player must:

- Create a remote game
- Share the URL of the game
- Share the code of the game room
- Insert your player's name and join

The guest players must:

- Click on the link of the game
- Go back at the place where the room code was shared and copy it
- Go back to the game's page and paste the code
- Insert your player's name and join

It's way too much! People just want to jump into to a game and start playing!

### TODO

Streamline the process of sharing the URL of the game so that only one click on the
link will put the players in to the join flow, requiring to only add their name and
play. Ideas:

Host side:

- Instead of showing the game id show the full URL with the game id as a parameter
- Add a copy link button so that I don't have to select and copy
- Automatically put the URL in the clipboard so that I don't have to click the copy button unless needed

Guest side:

- Open the link and automatically join a game if an id is present in the URL

## Second stage

Everyone still stayed even if the join process was not ideal, there were ten players
ready to start the game, we hit `Start!` and the game actually starts for real but
there were only five players visible and the discard and the refill decks were missing,
what is going on here? Someone starts playing anyway and the game was working for those
five players, at least, so I was mostly happy about the first demo, I promise to look
at it later and to make it ready for the first real match.

An evening later I've started investigating the issue, fortunately simulating many
players is easy to do because I can run a local game with as many players as I need.

Up to five players it works as expected, but at six I can reproduce the problem!
I suspect that something in the DOM is pushing some components out of the window, so I
jump to the inspector and I notice that while the container DIV elements are in place, the refill deck and discard piles are empty...and there are only five players' hand
container too, not six with a weird state...mmm

After a couple of minutes, while I am playing with the layout, I've had one of the
biggest facepalms of my life: there are not enough cards if the players are more than
five! There are 13 ranks (1 to 10 plus J, Q and K) and four suites which gives us 52
unique cards, each player start with 9 cards, so 9 times 6 = 54, not enough cards!

### TODO

Add enough cards given the amount of players in a game, possible solutions:

- Add more suites, I have to check the CSS Playing Cards to see how to do it
- Enable the same rank + suite to be present multiple times, it would be like using two sets of cards like we would do in real life

## UX

One thing that makes it really hard to play the game with many players is that not
all players' hand fits in one screen so you have to scroll the page to see them all,
which then hides the discard pile where the reference card is, totally not ideal.

### TODO

Try to always have all the hands displayed in a single screen not requiring to scroll.

## That's it for today

Feel free to try the [game](/demo/sillyhead) and have a look at the [sources](https://gitlab.com/fpischedda/mazeboard/demos), `sillyhead` namespace.