+++
title = "Sillyhead, second game built on AREX"
author = "FPSD"
date = 2024-06-09
tags = "arex"
+++

# Sillyhead

This is an (almost complete) implementation of the popular Shithead card game,
the rules are simple and can be found [here](https://en.wikipedia.org/wiki/Shithead_(card_game)).

Many thanks to the author of the [CSS Playing Cards](https://selfthinker.github.io/CSS-Playing-Cards/) project, it saved me sooo much time, and the cards are looking nice!

Try it [here](/demo/sillyhead).

Sources are available [here](https://gitlab.com/fpischedda/mazeboard/demos), `sillyhead` namespace.  

Like the first demo it can be played on a single browser or via network multiplayer.

## What's missing

- Re-arranging the cards before the game starts
- Eldest hand rule to decide who starts the game
- Quartets

## Additional rules

- Jack can be played on any card but the next reference card is the first non Jack or threes with hearts or diamonds suite
- Card with rank three and suite equal to hearts and diamonds, same as Jacks