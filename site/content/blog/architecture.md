+++
title = "Base game architecture"
author = "FPSD"
date = 2024-05-13
tags = "architecture"
+++

# Architecture

- Base game loop
- Exchanging messages
- Demo

## Base game loop

A usual game loop works like this:

- Check user input
- Updae game state
- Render view
- Repeat

This works well in a realtime single player games.

Multiplayer games must all consider many "viewers" of the game state, and multiple input sources that
contributes to the update of the shared game state.

A common approach is to handle local input and state changes locally, send the input to a central
source of truth and adjust the game state according to the global state if it diverges from the
local state. This accounts for the network delay and makes a game playable even with slow connections.

On the other hand, turn based games are a bit less demanding in terms of realtime updates.

A game loop can be changed to:

- Wait for user actions or commands
- According to the game logic handle actions and generate events representing game state changes
- Apply events (projections of how the state should change) to the game state
- Send events to clients so that they can update their view of the proposed state change

This approach may seem costly in a usual game with a 120Hz refresh rate but it proved to work quite nicely
for less demanding games.

One thing to consider is that the game state and view (UI) state are totally independent so the UI rendering
code can be as complicated and costly as you like, creating rich eye candy interfaces on the client side.

## How do these actions/events look like?

Actions and events are best represented as pure data, especially if we consider that this information
should be exchanged over a network.

Example actions:

```python
class Action(Enum):
    JoinGame = 0
    RollDice = 1

[Action.JoinGame, {'name': 'foo'}]
[Action.RollDice, {'die_ids': [2, 3]}]

# Base action handler
action = await receive_action()  # for example [Action.JoinGame, {'name': 'foo'}]
events = match action:
             case [Action.JoinGame, opts]:
                 generate_join_game_events(opts)
             case [Action.RollDice, opts]:
                 generate_roll_dice_events(opts)

send_events(events)
```

Or events:
```python
class Event(Enum):
    PlayerJoined = 0
    DiceRolled = 1
    GameOver = 2

[Event.PlayerJoined, {'client_id': '123', 'name': 'foo'}]
[Event.DiceRolled, {'faces': {'die_2': 4, 'die_3': 6}}]
[Event.GameOver {'winner': '123'}]

# Base events handler
for event in await receive_events():
    match event:
        [Event.PlayerJoined, opts]:
            handle_player_joined(opts)
```

So to recap:

- actions represent something that a player wants to do
- events represent changes to be applied to the game state
- event handlers update their view of the game (it may include UI state changes, storage services)

## Exchanging messages

At this point we have a kind of protocol based on Actions and Events to drive game and UI state
updates; it works nicely for a local game but (not) suprisingly, being data based (something that can be de/serialized),
it work over a network connection as well.

To break it down we have clients and servers.

Clients

- can send actions
- listen to events
- use events to update their state

Servers

- are the source of truth
- generate events to update their state according to user actions
- broadcast events so that clients can update thier view

All we need at this point is a central component that coordinates the exchange of the messages,
at the bare minimum it should offer:

- a way to create game "rooms"
- forward actions from clients to a game master
- broadcast events from the game master to clients
