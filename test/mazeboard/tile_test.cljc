(ns mazeboard.tile-test
  (:require

   #?(:cljs [cljs.test :refer [are deftest is testing]]
      :clj  [clojure.test :refer [are deftest is testing]])

   [mazeboard.tile :as tile]))

(def the-tile [:closed :closed :open :open])
(def closed-tile [:closed :closed :closed :closed])

(deftest tile
  (testing "Tiles is composed of four elements"
    (is (= (tile/make-tile :closed :closed :closed :closed) [:closed :closed :closed :closed])))
  (testing "I can get the tile wall at :north :east :south :west"
    (are [direction type]
        (= (tile/wall-at the-tile direction) type)

      :north :closed
      :east  :closed
      :south :open
      :west  :open))
  
  (testing "I can check if a tile wall is open or not"
    (are [direction open?]
        (= (tile/side-open? the-tile direction) open?)

      :north false
      :east  false
      :south true
      :west  true))
  
  (testing "I can fetch the names of open walls in a tile"
    (is (= (tile/open-walls the-tile) [:south :west])))

  (testing "Tiles can be rotated clockwise or counterclockwise"
    (are [walls direction]
        (= [:open :open :closed :closed] (tile/rotate-tile walls direction))
      [:closed :open :open :closed] :left 
      [:open :closed :closed :open] :right))
  
  (testing "Tiles should not have four closed walls"
    (is (= (some #{:open} (tile/fix-closed-walls closed-tile)) :open)))
  )
