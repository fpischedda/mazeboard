(ns sillyhead.ui.event-handler-test
  (:require [clojure.test :refer [deftest testing is]]
            [sillyhead.actions :as a]
            [sillyhead.ui.event-handler :as sut]))

(deftest top-up-to
  (testing "Should add values to the coll"
    (is (= [1 2 nil] (sut/top-up-to [1 2] 3 nil)))))

(deftest ui-player-hand-test
  (testing "Should convert from event player hand to ui player hand"
    (is (= {:show-hand false
            :name "name"
            :current-player false
            :player-idx 0
            :hand [{:rank 2 :suite :hearts}
                   {:rank 3 :suite :hearts}
                   {:rank 4 :suite :hearts}]
            :table [{:hidden  {:rank 5  :suite :hearts}
                     :visible {:rank 6  :suite :hearts}}
                    {:hidden  {:rank 7  :suite :hearts}
                     :visible {:rank 8  :suite :hearts}}
                    {:hidden  {:rank 9  :suite :hearts}
                     :visible {:rank 10 :suite :hearts}}]}

           (sut/event-hand->ui-hand
            {:hand [{:rank 2 :suite :hearts}
                    {:rank 3 :suite :hearts}
                    {:rank 4 :suite :hearts}]
             :table-hidden  [{:rank 5  :suite :hearts}
                             {:rank 7  :suite :hearts}
                             {:rank 9  :suite :hearts}]
             :table-visible [{:rank 6  :suite :hearts}
                             {:rank 8  :suite :hearts}
                             {:rank 10 :suite :hearts}]}

            {:name "name"
             :client-id "local"
             :player-idx 0}
            "non active client"))))

  (testing "Should show the player's hand"
    (is (= {:show-hand true
            :name "name"
            :current-player false
            :player-idx 0
            :hand [{:rank 2 :suite :hearts}
                   {:rank 3 :suite :hearts}
                   {:rank 4 :suite :hearts}]
            :table [{:hidden  {:rank 5  :suite :hearts}
                     :visible {:rank 6  :suite :hearts}}
                    {:hidden  {:rank 7  :suite :hearts}
                     :visible {:rank 8  :suite :hearts}}
                    {:hidden  {:rank 9  :suite :hearts}
                     :visible {:rank 10 :suite :hearts}}]}

           (sut/event-hand->ui-hand
            {:hand [{:rank 2 :suite :hearts}
                    {:rank 3 :suite :hearts}
                    {:rank 4 :suite :hearts}]
             :table-hidden  [{:rank 5  :suite :hearts}
                             {:rank 7  :suite :hearts}
                             {:rank 9  :suite :hearts}]
             :table-visible [{:rank 6  :suite :hearts}
                             {:rank 8  :suite :hearts}
                             {:rank 10 :suite :hearts}]}

            {:name "name"
             :client-id "local"
             :player-idx 0}
            "local"))))

  (testing "Should top up with nil if short of table visible cards"
    (is (= {:show-hand false
            :name "name"
            :current-player false
            :player-idx 0
            :hand [{:rank 2 :suite :hearts}
                   {:rank 3 :suite :hearts}
                   {:rank 4 :suite :hearts}]
            :table [{:hidden  {:rank 5  :suite :hearts}
                     :visible {:rank 6  :suite :hearts}}
                    {:hidden  {:rank 7  :suite :hearts}
                     :visible {:rank 8  :suite :hearts}}
                    {:hidden  {:rank 9  :suite :hearts}
                     :visible nil}]}

           (sut/event-hand->ui-hand
            {:hand [{:rank 2 :suite :hearts}
                    {:rank 3 :suite :hearts}
                    {:rank 4 :suite :hearts}]
             :table-hidden  [{:rank 5  :suite :hearts}
                             {:rank 7  :suite :hearts}
                             {:rank 9  :suite :hearts}]
             :table-visible [{:rank 6  :suite :hearts}
                             {:rank 8  :suite :hearts}]}
            
            {:name "name"
             :client-id "local"
             :player-idx 0}
            "non active client"))))
  )

(deftest handle-game-started-test
  (let [event (first (a/action--start-game->events {:players [{:name "1"}
                                                              {:name "2"}]}))
        ui-state {:state :game-state/initial}
        ui-state (sut/handle-event ui-state event)]
    (testing "UI state is started"
      (is (= :game-state/started (:state ui-state))))
    (testing "There are two palyers"
      (is (= 2 (count (:players ui-state)))))
    (testing "There is one card in the discard pile"
      (is (= 1 (count (get-in ui-state [:board :table :discard-pile])))))
    (testing "There are the expected number of cards in the deck"
      (is (= (- 52 1 18) (count (get-in ui-state [:board :table :deck])))))
    (testing "First player has three cards in the hand"
      (is (= 3 (count (get-in ui-state [:board :players-hand 0 :hand])))))
    (testing "First player has three cards in the table"
      (is (= 3 (count (get-in ui-state [:board :players-hand 0 :table])))))
    ))

(deftest handle-event--card-played-test
  (testing "Should update discard pile and players' hand correctly"
    (let [state {:client-id "local"
                 :players [{:client-id "local" :name "one" :player-idx 0}]
                 :board {:table {:discard-pile []}
                         :players-hand [{:hand [{:rank 3 :suite :clubs}]
                                         :table [{:hidden nil :visible nil}
                                                 {:hidden nil :visible nil}
                                                 {:hidden nil :visible nil}]
                                         :show-hand true
                                         :current-player false
                                         :name "one"
                                         :player-idx 0}]}}
          new-state
          (-> state
              (update-in [:board :table :discard-pile] conj {:rank 3 :suite :clubs})
              (assoc-in [:board :players-hand 0 :hand] []))]
      (is (= new-state
             (sut/handle-event
              state
              [:card-played {:discard-pile [{:rank 3 :suite :clubs}]
                             :players-hand [{:hand []
                                             :table-visible []
                                             :table-hidden []}]}]))))))

(deftest handle-event--player-left
  (testing ":player-left event should set :is-out? to the correct player"
    (is (= {:players [{:name "one"} {:name "two" :is-out? true}]}
           (sut/handle-event
            {:players [{:name "one"} {:name "two"}]}
            [:player-left {:player 1}])))))
